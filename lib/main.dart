import 'package:flutter/material.dart';
import 'package:quiz_app/QuizBrain.dart';

void main() {
  runApp(QuizApp());
}

class QuizApp extends StatefulWidget {
  @override
  _QuizAppState createState() => _QuizAppState();
}

class _QuizAppState extends State<QuizApp> {
  QuizBrain quizBrain = QuizBrain();
  List<Icon> scoreKeeper = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        home: Scaffold(
          backgroundColor: Colors.white24,
          appBar: AppBar(
            title: Text('Quiz App'),
          ),
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: Text(
                        quizBrain.getQuestionText(),
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: FlatButton(
                    padding: EdgeInsets.all(15.0),
                    textColor: Colors.white,
                    color: Colors.green,
                    child: Text(
                      'True',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {
                      checkAnswer(true);
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  flex: 0,
                  child: FlatButton(
                    padding: EdgeInsets.all(15.0),
                    textColor: Colors.white,
                    color: Colors.red,
                    child: Text(
                      'False',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {
                      checkAnswer(false);
                    },
                  ),
                ),
                Row(
                  children: scoreKeeper,
                )
              ],
            ),
          ),
        ));
  }

  void checkAnswer(bool checkAnswer) {
    bool correctAnswer = quizBrain.getQuestionAnswer();
    setState(() {
      if(checkAnswer == correctAnswer){
        scoreKeeper.add(Icon(Icons.check, color: Colors.green,));
      }else{
        scoreKeeper.add(Icon(Icons.close, color: Colors.red,));
      }

      quizBrain.nextQuestion();
    });
  }
}
