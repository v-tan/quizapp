import 'package:quiz_app/Question.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class QuizBrain{
  int _questionNumber = 0;
  List<Question> _questionBank = [
    Question('Prince Harry is taller than Prince William', false),
    Question('The star sign Aquarius is represented by a tiger', true),
    Question('Leonardo Dicaprio has won two Academy Awards', false),
    Question('Marrakesh is the capital of Morocco', false),
    Question('Idina Menzel sings \'let it go\' 22 times in \'Let It Go\' from Frozen', false),
  ];

  String getQuestionText(){
    return _questionBank[_questionNumber].questionText;
  }

  bool getQuestionAnswer(){
    return _questionBank[_questionNumber].questionAnswers;
  }

  void nextQuestion(){
    if(_questionNumber < _questionBank.length - 1){
      _questionNumber++;
    }else{
      print("Quiz is finished");
    }
  }
}